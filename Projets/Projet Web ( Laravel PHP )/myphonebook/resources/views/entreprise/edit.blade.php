@extends("layouts.default")

@section('main')
    <a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
    <a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
    <a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>
    <form action="{{route('entreprise.update', $entreprise->id)}}" method="POST">
        @csrf

        @method('PUT')

        <input type="text" placeholder='name' name="nom" required="required" value='{{$entreprise->nom}}' >

        <input type="text" placeholder='rue' name="rue" required="required" value='{{$entreprise->rue}}'>

        <input type="text" minlength="5" maxlength="5" placeholder='code postal' name="code_postal" required="required" pattern="[0-9]*" value='{{$entreprise->code_postal}}' >

        <input type="text" placeholder='ville' name="ville" required="required" value='{{$entreprise->ville}}'>

        <input type="text" placeholder='tel' name="tel" minlength="10" maxlength="10" pattern="[0-9]*" value='{{$entreprise->tel}}'>

        <input type="email" placeholder='mail' name="mail" required='requiered' value='{{$entreprise->mail}}'> 

        <input type="submit" value="Modifier" >
    </form>
    <a href="http://127.0.0.1:8000/entreprise"><button>liste des enterprises</button></a>
@endsection