@extends("layouts.default")

@section('main')
<a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
<a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
<a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>
<ul>
        <li class='entli'>nom</li>
        <li class='entli'>tel</li>
        <li class='entli'>mail</li>
        <li class='entli'>code_postal</li>
    </ul>
    <ul>
        <li class='entli'> {{$entreprise->nom}}</li>
        <li class='entli'>0{{$entreprise->tel}}</li>
        <li class='entli'> {{$entreprise->mail}}</li>
        <li class='entli'> {{$entreprise->code_postal}}</li>
    </ul>

    @can('create', $entreprise)
                <a href="{{route('entreprise.edit',$entreprise->id)}}"><button>Modifier</button></a>
    @endcan
    <div class='oui'></div>
    @can('delete', $entreprise)
<form action="{{route('entreprise.destroy',$entreprise->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit">Supprimer</button>
    </form>
    @endcan
    <br>
    <a href="http://127.0.0.1:8000/entreprise"><button>liste des enterprises</button></a>
@endsection