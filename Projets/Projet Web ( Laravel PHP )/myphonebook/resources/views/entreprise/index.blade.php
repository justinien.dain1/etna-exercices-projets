
@extends("layouts.default")

@section('main')
    <a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
    <a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
    <a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>

    <ul>
        <li class='entli'>nom</li>
        <li class='entli'>tel</li>
        <li class='entli'>mail</li>
        <li class='entli'>code_postal</li>
        <li class='entli'>enteprise_id</li>
    </ul>
    @foreach($entreprises as $entreprise)
    <ul>
        <li class='entli'> {{$entreprise->nom}}</li>
        <li class='entli'>0{{$entreprise->tel}}</li>
        <li class='entli'> {{$entreprise->mail}}</li>
        <li class='entli'> {{$entreprise->code_postal}}</li>
        <li class='entli'> {{$entreprise->id}}</li>
        <li class='entli'><a href="http://127.0.0.1:8000/entreprise/{{$entreprise->id}}"><button>VOIR</button></a></li>
    </ul>
    @endforeach
    @can('create', $entreprise)

        <a href="http://127.0.0.1:8000/entreprise/create"><button>ajouter une entreprises</button></a>
    @endcan
    <div class='oui'></div>
    <form action="{{route('entreprise.index')}}" method="get">
    @csrf
    <h3>Recherche</h3>
        <input type="text" placeholder='nom' name="nom" >
        <input type="text" placeholder='ville' name="ville" >
        <input type="email" placeholder='email' name="email" >
        <input type="submit" value="chercher" >
    </form>

    @if($nom !=="")
    @foreach($entreprises as $entreprise)
        @if($entreprise->nom === $nom)
            <ul>
                <li class='entli'> {{$entreprise->nom}}</li>
                <li class='entli'>0{{$entreprise->tel}}</li>
                <li class='entli'> {{$entreprise->mail}}</li>
                <li class='entli'> {{$entreprise->code_postal}}</li>
                <li class='entli'> {{$entreprise->id}}</li>
                <li class='entli'><a href="http://127.0.0.1:8000/entreprise/{{$entreprise->id}}"><button>VOIR</button></a></li>
            </ul>
        @endif
    @endforeach
    @endif

    @if($ville !=="")
    @foreach($entreprises as $entreprise)
        @if($entreprise->ville === $ville)
            <ul>
                <li class='entli'> {{$entreprise->nom}}</li>
                <li class='entli'>0{{$entreprise->tel}}</li>
                <li class='entli'> {{$entreprise->mail}}</li>
                <li class='entli'> {{$entreprise->code_postal}}</li>
                <li class='entli'> {{$entreprise->id}}</li>
                <li class='entli'><a href="http://127.0.0.1:8000/entreprise/{{$entreprise->id}}"><button>VOIR</button></a></li>
            </ul>
        @endif
    @endforeach
    @endif

    @if($email !=="")
    @foreach($entreprises as $entreprise)
        @if($entreprise->mail === $email)
            <ul>
                <li class='entli'> {{$entreprise->nom}}</li>
                <li class='entli'>0{{$entreprise->tel}}</li>
                <li class='entli'> {{$entreprise->mail}}</li>
                <li class='entli'> {{$entreprise->code_postal}}</li>
                <li class='entli'> {{$entreprise->id}}</li>
                <li class='entli'><a href="http://127.0.0.1:8000/entreprise/{{$entreprise->id}}"><button>VOIR</button></a></li>
            </ul>
        @endif
    @endforeach
    @endif

@endsection