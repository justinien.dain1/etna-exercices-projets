@extends("layouts.default")

@section('main')
<a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
<a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
    <a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>
    <form action="{{route('collab.update',$collaborateur->id)}}" method="post">
        @csrf
        @method('PUT')
        <label for="civility">Homme</label>
        <input type="radio" id="homme" name="civility" value="homme">
        <label for="civility">Femme</label>
        <input type="radio" id="femme" name="civility" value="femme">
        <label for="civility">Non-Binaire</label>
        <input type="radio" id="nonbinaire" name="civility" value="nonbinaire">

        <input type="text" placeholder='nom' name="nom" required="required" value='{{$collaborateur->nom}}' > 

        <input type="text" placeholder='prenom' name="prenom" required="required" value='{{$collaborateur->prenom}}' > 

        <input type="text" placeholder='rue' name="rue" required="required" value='{{$collaborateur->rue}}'> 

        <input type="text" minlength="5" maxlength="5" placeholder='code postal' name="code_postal" required="required" pattern="[0-9]*" value='{{$collaborateur->code_postal}}' >

        <input type="text" placeholder='ville' name="ville" required="required" value='{{$collaborateur->ville}}'>

        <input type="text" placeholder='tel' name="tel" minlength="10" maxlength="10" pattern="[0-9]*" value='{{$collaborateur->tel}}'> 

        <input type="email" placeholder='mail' name="mail" required='requiered' value='{{$collaborateur->mail}}'> 

        <input type="number" placeholder='entreprise' name="entreprise" required="required" value='{{$collaborateur->entreprise_id}}' >

        <input type="submit" value="Modifier" >
    </form>
    <a href="http://127.0.0.1:8000/collab"><button>liste des collaborateurs</button></a>
@endsection