@extends("layouts.default")

@section('main')
<a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
<a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
<a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>

    <ul>
        <li class='entli'> nom</li>
        <li class='entli'> prenom</li>
        <li class='entli'>tel</li>
        <li class='entli'>mail</li>
        <li class='entli'>entrepriseID</li>
    </ul>
    <ul>
        <li class='entli'> {{$collaborateur->nom}}</li>
        <li class='entli'> {{$collaborateur->prenom}}</li>
        <li class='entli'>0{{$collaborateur->tel}}</li>
        <li class='entli'> {{$collaborateur->mail}}</li>
        <li class='entli'> {{$collaborateur->entreprise_id}}</li>
    </ul>
    @can('create', $collaborateur)
    <a href="{{route('collab.edit',$collaborateur->id)}}"><button>Modifier</button></a>
    @endcan
    <div class='oui'>
        
    </div>
    @can('delete', $collaborateur)
    <form action="{{route('collab.destroy',$collaborateur->id)}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit">Supprimer</button>
    </form>
    @endcan
    <br>
    <a href="http://127.0.0.1:8000/collab"><button>liste des collaborateurs</button></a>

@endsection