@extends("layouts.default")

@section('main')
    <a href="http://127.0.0.1:8000/menu"><button>accueil</button></a>
    <a href="http://127.0.0.1:8000/entreprise"><button>entreprises</button></a>
    <a href="http://127.0.0.1:8000/collab"><button>collaborateurs</button></a>
    <ul>
        <li class='entli'> nom</li>
        <li class='entli'> prenom</li>
        <li class='entli'>tel</li>
        <li class='entli'>mail</li>

        <li class='entli'>entreprise</li>
        <li class='entli'>tel_entreprise</li>
    </ul>
    @foreach($collaborateurs as $collaborateur)
    <ul>
        <li class='entli'> {{$collaborateur->nom}}</li>
        <li class='entli'> {{$collaborateur->prenom}}</li>
        <li class='entli'>0{{$collaborateur->tel}}</li>
        <li class='entli'> {{$collaborateur->mail}}</li>
        @foreach($entreprises as $entreprise)
            @if($entreprise->id === $collaborateur->entreprise_id)
                
                <li class='entli'>{{ $entreprise->nom }}</li>
                <li class='entli'>0{{ $entreprise->tel }}</li>
            @endif
        @endforeach
        <li class='entli'><a href="http://127.0.0.1:8000/collab/{{$collaborateur->id}}"><button>VOIR</button></a></li>
    </ul>
    @endforeach

    @can('create', $collaborateur)
    <a href="http://127.0.0.1:8000/collab/create"><button>ajouter un collaborateur</button></a>
    @endcan
@endsection