<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body{
            background-color: #181A1B;
            color : white;
        }
        .oui{
            height: 10px;
            

        }
        button{
            background-color: white;
            border:none;
            font-family: cursive
            
        }
        button:hover{
            background-color: black;
            color:white;
            cursor:pointer;
            border:none;
        }
        ul{
            padding-left : 0px;
        }
        .entli{
            display: inline-block;
            padding-left:10%;
            width:50px;
        }
    </style>
    <title>Document</title>
</head>
<body>
    <h1>My phone Book</h1>
    
    <main>
    
    @yield('main')
    </main>
    
</body>
</html>