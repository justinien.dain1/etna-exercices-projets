<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollaborateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborateurs', function (Blueprint $table) {
            $table->id();
            $table->string('civility');
            $table->string('nom');
            $table->string('prenom');
            $table->string('rue');
            $table->integer('code_postal');
            $table->string('ville');
            $table->integer('tel')->unique();
            $table->string('mail')->unique();
            $table->foreignId('entreprise_id')->constrained();
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborateurs');
    }
}
