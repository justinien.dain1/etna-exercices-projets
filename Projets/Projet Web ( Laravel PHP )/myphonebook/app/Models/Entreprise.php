<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class entreprise extends Model
{
    use HasFactory;

    protected $fillable= [
        'nom',
        'rue',
        'code_postal',
        'ville',
        'tel',
        'mail'
    ];




    protected $table = 'entreprises';

    public function collaborateurs(){
        return $this->hasMany(Collaborateur::class);
    }
}
