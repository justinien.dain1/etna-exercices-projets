<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class collaborateur extends Model
{
    use HasFactory;
    protected $fillable=[
        'civility',
        'nom',
        'prenom',
        'rue',
        'code_postal',
        'ville',
        'tel',
        'mail',
        'entreprise'
    ];

    public function entreprise(){
        return $this->belongsTo(Entreprise::class,'entreprise_id');
    }




    protected $table = 'collaborateurs';
}
