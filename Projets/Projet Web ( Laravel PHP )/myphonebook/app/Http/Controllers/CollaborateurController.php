<?php

namespace App\Http\Controllers;

use App\Models\Collaborateur;
use App\Models\Entreprise;
use Illuminate\Http\Request;

class CollaborateurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $this->authorize('viewAny', Collaborateur::class);
        $collaborateurs = Collaborateur::all();
        $entreprises = Entreprise::all();
        //dd($entreprises);
         return view('collaborateur.index',[
             'collaborateurs'=>$collaborateurs,
             'entreprises'=>$entreprises
         ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Collaborateur::class);
        return view('collaborateur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Collaborateur::class);
        $validated = $request->validate([
            'civility' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'rue' => 'required',
            'code_postal' => 'required',
            'ville' => 'required',
            'tel' => '',
            'mail' => 'required',
            'entreprise' => 'required',
        ]);

        $collaborateur = new Collaborateur;
        
        $collaborateur->civility = $validated["civility"];
        $collaborateur->nom = $validated["nom"];
        $collaborateur->prenom = $validated["prenom"];
        $collaborateur->rue = $validated["rue"];
        $collaborateur->code_postal = $validated["code_postal"];
        $collaborateur->ville = $validated["ville"];
        $collaborateur->tel = $validated["tel"];
        $collaborateur->mail = $validated["mail"];
        $collaborateur->entreprise_id = $validated["entreprise"];
        
        $collaborateur->save();
        return redirect()->route('collab.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Collaborateur::class);
        $collaborateur = Collaborateur::find($id);
        return view('collaborateur.show',
        ['collaborateur'=>$collaborateur]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Collaborateur $collab)
    {
        $this->authorize('create', Collaborateur::class);
       return view('collaborateur.edit',
       ['collaborateur'=>$collab]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collaborateur $collab)
    {
        $this->authorize('create', Collaborateur::class);
        $validated = $request->validate([
            'civility' => 'required',
            'nom' => 'required',
            'prenom' => 'required',
            'rue' => 'required',
            'code_postal' => 'required',
            'ville' => 'required',
            'tel' => '',
            'mail' => 'required',
            'entreprise' => 'required',
        ]);
        $collab->civility = $validated["civility"];
        $collab->nom = $validated["nom"];
        $collab->prenom = $validated["prenom"];
        $collab->rue = $validated["rue"];
        $collab->code_postal = $validated["code_postal"];
        $collab->ville = $validated["ville"];
        $collab->tel = $validated["tel"];
        $collab->mail = $validated["mail"];
        $collab->entreprise_id = $validated["entreprise"];
        
        $collab->save();
        return redirect()->route('collab.index');
        //dd($validated);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collaborateur $collab)
    {
        $this->authorize('delete', Collaborateur::class);
        $collab->delete();
        return redirect()->route('collab.index');
    }
}
