<?php

namespace App\Http\Controllers;


use App\Models\Entreprise;
use Illuminate\Http\Request;


class EntrepriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('viewAny', Entreprise::class);
        $entreprises = Entreprise::all();
        
        $nom = $request->input('nom');
        $ville = $request->input('ville');
        $email = $request->input('email');
        //dd($entreprises);
         return view('entreprise.index',[
             'entreprises'=>$entreprises,
             'nom'=>$nom,
             'ville'=>$ville,
             'email'=>$email
         ]);
         dd($request);
        //return view('entreprise.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Entreprise::class);
        return view('entreprise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Entreprise::class);
        $validated = $request->validate([
            'nom' => 'required',
            'rue' => 'required',
            'code_postal' => 'required',
            'ville' => 'required',
            'tel' => '',
            'mail' => 'required',
        ]);
       $entreprise = new Entreprise;

       $entreprise->nom = $validated["nom"];
       $entreprise->rue = $validated["rue"];
       $entreprise->code_postal = $validated["code_postal"];
       $entreprise->ville = $validated["ville"];
       $entreprise->tel = $validated["tel"];
       $entreprise->mail = $validated["mail"];

       $entreprise->save();
       return redirect()->route('entreprise.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('viewAny', Entreprise::class);
        $entreprise = Entreprise::find($id);
        return view('entreprise.show', 
        ['entreprise'=>$entreprise]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Entreprise $entreprise)
    {
        $this->authorize('create', Entreprise::class);
        return view('entreprise.edit', 
        ['entreprise'=> $entreprise]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entreprise $entreprise)
    {
        $this->authorize('create', Entreprise::class);
        $validated = $request->validate([
           'nom' => 'required',
           'rue' => 'required',
           'code_postal' => 'required',
           'ville' => 'required',
           'tel' => '',
           'mail' => 'required',
       ]);

       ////EntrepriseController::create($validated);

       $entreprise->nom = $validated["nom"];
       $entreprise->rue = $validated["rue"];
       $entreprise->code_postal = $validated["code_postal"];
       $entreprise->ville = $validated["ville"];
       $entreprise->tel = $validated["tel"];
       $entreprise->mail = $validated["mail"];

       $entreprise->save();
    
       return redirect()->route('entreprise.show',['entreprise'=>$entreprise->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entreprise $entreprise)
    {
        $this->authorize('delete', Entreprise::class);
        $entreprise->delete();
        return redirect()->route('entreprise.index');
    }
}
