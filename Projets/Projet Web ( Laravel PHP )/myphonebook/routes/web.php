<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EntrepriseController;
use App\Http\Controllers\CollaborateurController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/menu', function () {
    return view('greeting');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');



Route::get('/test', function () {
    return 'test';
});

Route::resource('entreprise', EntrepriseController::class);
Route::resource('collab', CollaborateurController::class);

Route::get('/admin',function(){
    if (! Gate::allows('access-admin')) {
        return 'non';
    }
    return 'admin';
});

require __DIR__.'/auth.php';
