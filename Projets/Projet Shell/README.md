Lors de la première semaine nous avons étudié le Shell.

Le projet qui était demandé de faire avec ce langage était de faire
un jeu de rôle sur l'univer de Link en tour / tour dans le terminal;

Pour lancer le jeu il faut cloner le dossier ( base game) contenant le projet et d'aller dans le terminal.

Une fois dans le terminal et dans le dossié copier cette ligne : 

./hyrule_castle.sh players.csv bosses.csv enemies.csv 

Et voila vous pouvez jouer au jeu.

le jeu ce déroule de la maniere suivante :

le jeu se déroule de la manière suivante :

- Chaque personnage possède des dégâts d'attaque / vie différents.
- Le heal vous rend 50% de la vie maximale.

- Votre personnage sera choisi aléatoirement.
- Le monstre qu'il va rencontrer durant les 10 premiers étages est lui aussi choisi aléatoirement.
- Le boss qu'il affronteras un fois les 10 étages gagné est lui aussi choisi aléatoirement.

le dossié mod contient des mods mais il ne sont pas encore implémentés dans le jeu.
