#!/bin/bash

# Etage ( floor )
cfloor=1
maxfloor=10


hero_file=$1 #players.csv  #fichier players.csv
first_line=0

#stats du héro ( $l.. )
state_player() {
lname=$name
lhp=$hp
lhpmax=$hp
lstr=$str
heal=$((lhpmax / 2))
}
# Choix du player de maniere random
randomChoice=$(($RANDOM%100))
while IFS=',' read -r id name hp mp str int def res spd luck race class raritys ; do

    if [[ $randomChoice -le 49 ]] && [[ $raritys -eq 1 ]];then
	state_player
  elif [[ $randomChoice -ge 50 ]] && [[ $randomChoice -le 79 ]] && [[ $raritys -eq 2 ]];then
	state_player
  elif [[ $randomChoice -ge 80 ]] && [[ $randomChoice -le 94 ]] && [[ $raritys -eq 3 ]];then
	state_player
  elif [[ $randomChoice -ge 95 ]] && [[ $randomChoice -le 99 ]] && [[ $raritys -eq 4 ]];then
	state_player
elif [[ $randomChoice -eq 100 ]] && [[ $raritys -eq 5 ]];then
	state_player
 else
        first_line=1
    fi
done < $hero_file

boss_file=$2 #bosses.csv #fichier booses.csv
first_lineb=0
enemy_file=$3 #enemies.csv  #fichier enemy.csv
first_linee=0

#stat enemy ( e...)
stat_enemy() {
        ename=$name
        ehp=$hp
	ehpmax=$hp
        estr=$str
}
# choix de l'enemi de maniere random
randomChoicee=$(($RANDOM%200))

while IFS=',' read -r id name hp mp str int def res spd luck race class raritys ; do

    if [[ $randomChoicee -ge 0 ]] && [[ $randomChoicee -le 24 ]] && [[ $raritys -eq 1 ]] && [[ $id -eq 7 ]];then
                stat_enemy
        elif [[ $randomChoicee -ge 25 ]] && [[ $randomChoicee -le 49 ]] && [[ $raritys -eq 1 ]] && [[ $id -eq 10 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 50 ]] && [[ $randomChoicee -le 74 ]] && [[ $raritys -eq 1 ]] && [[ $id -eq 11 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 75 ]] && [[ $randomChoicee -le 99 ]] && [[ $raritys -eq 1 ]] && [[ $id -eq 12 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 100 ]] && [[ $randomChoicee -le 129 ]] && [[ $raritys -eq 2 ]] && [[ $id -eq 6 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 130 ]] && [[ $randomChoicee -le 159 ]] && [[ $raritys -eq 2 ]] && [[ $id -eq 9 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 160 ]] && [[ $randomChoicee -le 174 ]] && [[ $raritys -eq 3 ]] && [[ $id -eq 1 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 175 ]] && [[ $randomChoicee -le 189 ]] && [[ $raritys -eq 3 ]] && [[ $id -eq 8 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 190 ]] && [[ $randomChoicee -le 193 ]] && [[ $raritys -eq 4 ]] && [[ $id -eq 3 ]];then
                 stat_enemy
        elif [[ $randomChoicee -ge 194 ]] && [[ $randomChoicee -le 197 ]] && [[ $raritys -eq 4 ]] && [[ $id -eq 4 ]];then
                 stat_enemy
        elif [[ $randomChoicee -eq 198 ]] && [[ $raritys -eq 5 ]] && [[ $id -eq 2 ]];then
                 stat_enemy
        elif [[ $randomChoicee -eq 199 ]] && [[ $raritys -eq 5 ]] && [[ $id -eq 5 ]];then
                 stat_enemy
        else
                first_linee=1
        fi

done < $enemy_file
#stat boss ( b... )
stat_boss() {
        bname=$name
        bhp=$hp
	bhpmax=$hp
        bstr=$str
}

randomChoiceb=$(($RANDOM%200))
# choix du boss de maniere random
while IFS=',' read -r id name hp mp str int def res spd luck race class raritys ; do

    if [[ $randomChoiceb -le 99 ]] && [[ $raritys -eq 1 ]];then
        stat_boss
    elif [[ $randomChoiceb -ge 100 ]] && [[ $randomChoiceb -le 159 ]] && [[ $raritys -eq 2 ]];then
        stat_boss
     elif [[ $randomChoiceb -ge 160 ]] && [[ $randomChoiceb -le 174 ]] && [[ $raritys -eq 3 ]] && [[ $id -eq 2 ]];then
        stat_boss
     elif [[ $randomChoiceb -ge 175 ]] && [[ $randomChoiceb -le 189 ]] && [[ $raritys -eq 3 ]] && [[ $id -eq 5 ]];then
        stat_boss
     elif [[ $randomChoiceb -ge 190 ]] && [[ $randomChoiceb -le 193 ]] && [[ $raritys -eq 4 ]] && [[ $id -eq 3 ]];then
        stat_boss
     elif [[ $randomChoiceb -ge 194 ]] && [[ $randomChoiceb -le 197 ]] && [[ $raritys -eq 4 ]] && [[ $id -eq 6 ]];then
        stat_boss
     elif [[ $randomChoiceb -ge 198 ]] && [[ $randomChoiceb -le 199 ]] && [[ $raritys -eq 5 ]];then
        stat_boss
    else
        first_lineb=1
    fi

done < $boss_file



#fuite="4. Escape"
#fuir=0
#block="3. Protect"
#blocked=1
while [[ $cfloor -lt "$maxfloor" ]] && [[ $fuir -eq 0 ]]; do   # permet de faire les combats d'étage en boucle en fonction du max floor )
ehp=$ehpmax   # remet les hp du monstre de l'étage à 100%
echo
echo " You encounter a $ename"
echo 
	while [[ $ehp -gt 0 ]] && [[ $lhp -gt 0 ]] && [[ $fuir -eq 0 ]] ; do # boucle qui permet de faire le combat temps que l'un des 2 participants meurt ou que le héro fuis
		echo "========== FLOOR $cfloor =========="
		echo  $lname  # afficher le nom du héro
		echo  $lhp / $lhpmax # afficher les hp du héro par rapport à ses hp max
		echo
		echo  $ename # afficher le nom de l'énemie
		echo  $ehp / $ehpmax #afficher les hp de l'énemi par rapport à ses hp 
		echo
		echo "---Options----------"
		echo " 1. Attack  2. Heal"  #afficher le choix pendant le combat 
		echo " $block $fuite "
		echo
		read -s rep # lire le choix ( -s permet de ne pas afficher )


		if [[ $rep -eq 1 ]]; then  # si choix 1 attaque
			ehp=$(($ehp-$lstr)) # calcul des hp du monstre apres l'attaque du héro
			echo "You attacked and dealt $lstr damages!"
				if [[ $ehp -le 0 ]]; then
                                	echo $ename" died!"
					break # permet de sortir de la condition pour pas que l'énemi tape
				fi
			lhp=$(($lhp-$estr)) # calul des hp du héro apres l'attaque du monstre
			echo $ename " attacked and dealt $estr damages!"
				if [[ $lhp -le 0 ]]; then # si le héro meurt
					echo $lname" died!"
					exit # permet de quitter le jeu
				fi
		fi
		if [[ $rep -eq 2 ]]; then  # si choix heal
			echo "You used heal!"
			lhp=$(($lhp+$heal)) # calcul des hp apres le heal
				if [[ $lhp -gt $lhpmax ]]; then # si le heal rend plus que les pv max  remet au pv max
                       		 lhp=$lhpmax
                		fi
			lhp=$(($lhp-$estr))
                	 echo $ename" attacked and dealt $estr damages!"
               	        if [[ $lhp -le 0 ]]; then
                                        echo $lname" died!"
                                        exit
                                fi

			 echo

		fi
		if [[ $rep -eq 4 ]] && [[ $fuir -eq 0 ]]; then  # si choix fuir
			echo "You run away like a coward"
			echo "..."
			echo "You are the shame of the village"
			exit
		fi
		if [[ $rep -eq 3 ]] && [[ $blocked -eq 1 ]]; then # si choix bloquer
                        echo "Vous bloquez la prochaine attaque"
                        dmgblock=$((estr / 2))  # calcul des nouveau dmg en fonction de l attaque de l'enemni
			lhp=$(($lhp-$dmgblock))
                        echo $ename " attacked and dealt $dmgblock damages!"
                                if [[ $lhp -le 0 ]]; then
                                        echo $lname" died!"
                                        exit
                                fi
                fi

	done


cfloor=$(($cfloor + 1)) # rajoute 1 a l'étage courrant

done


if [[ $cfloor -eq $maxfloor ]]; then  # si l'étage courrant est égal a étage 

while [[ $bhp -gt 0 ]] && [[ $lhp -gt 0 ]]; do # boucle qui permet de faire le combat temps que l'un des 2 participants meurt ( pas de fuite contre les boss )
		echo
                echo "========== FIGHT $bname =========="
                echo  $lname
                echo  $lhp / $lhpmax
                echo 
                echo  $bname
                echo  $bhp / $bhpmax
                echo
                echo "---Options----------"
                echo " 1. Attack   2.Heal  $block"
                echo
                read -s rep


                if [[ $rep -eq 1 ]]; then
                        bhp=$(($bhp-$lstr))
                        echo "You attacked and dealt $lstr damages!"
                                if [[ $bhp -le 0 ]]; then
                                        echo $bname" died"
					echo "Congratulation !!!" # si le boss meurt écrit un message 
                                        break
                                fi

                        lhp=$(($lhp-$bstr))
                        echo $bname " attacked and dealt $bstr damages!"
                        echo
                                if [[ $lhp -le 0 ]]; then
                                        echo $lname" died!"
                                        break
                                fi
                fi
                if [[ $rep -eq 2 ]]; then
                        echo "You used heal!"
                        lhp=$(($lhp+$heal))
				if [[ $lhp -gt $lhpmax ]]; then
                                 lhp=$lhpmax
                                fi
                        lhp=$(($lhp-$bstr))
                         echo $bname" attacked and dealt $bstr damages!"
                        echo
                fi
		if [[ $rep -eq 3 ]] && [[ $blocked -eq 1 ]]; then
                        echo "Vous bloquez la prochaine attaque"
                        echo $bstr
			bdmgblock=$((bstr / 2))
                        echo $bdmgblock
			lhp=$(($lhp-$bdmgblock))
                        echo $bname " attacked and dealt $bdmgblock damages!"
                                if [[ $lhp -le 0 ]]; then
                                        echo $lname" died!"
                                        exit
                                fi
                fi

done
fi

