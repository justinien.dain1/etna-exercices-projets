/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
**              No file there, just an etna header example
*/

#include <stdio.h>
#include <stdlib.h>
#include "my.h"


char    **get_data(int *key)
{
    char    **table = NULL;
    size_t  n = 0;
    char    *line = NULL;

    if (getline(&line, &n, stdin) == -1){
        free(line);
        return (NULL);
    }
    table = my_str_to_word_array(line);
    if (table[1] != NULL){
    }else {
    }


    if (table != NULL){
        *key = my_getnbr(table[0]);
    }else{
    }
    return (table);
}

void manage_list(int key, char *value, linked_list_t **list)
{
    linked_list_t *find_f = NULL;

    find_f = my_find_node(*list, key);
    if (value == NULL || value[0] == 'D') {
        if (find_f == NULL) {
                 my_putstr("-1 \n");
        } else {
                 my_putstr(find_f->value);
                 if (value != NULL)
                    my_delete_nodes(list, key);
        }
    } else {
        my_putnbr(key);
        my_putstr("\n");
        if (find_f == NULL) {
            value = my_strdup(value);
            *list = my_push_front_to_list(value, key, *list);
        } else {
            find_f->value = my_strdup(value);
        }
    }
}


void free_list(linked_list_t *list)
{
    linked_list_t *stock = NULL;

    if (!list)
        return;
    while (list->next != NULL){
        stock = list->next;
        free(list);
        list=stock;
    }
    free (list);
}



int     main(void)
{
    char            **table = NULL;
    int             key = 0;
    int             i = 0;
    linked_list_t   *list = NULL;
    
    while ((table = get_data(&key)) != NULL) {
        i = 0;
        manage_list(key, table[1], &list);
         while (table[i] != NULL){
             free(table[i]);
             i++;
         }
         free(table);
     }
     free_list(list);
    return (0);
}