/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
**              No file there, just an etna header example
*/

#include <stdio.h>
#include <stdlib.h>
#include "my.h"

void principal(void)
{
     char *l = NULL;
     char **table;
     char *data = NULL;
     int key = 0;
     size_t n = 0;
     linked_list_t *list = NULL;

     while (getline(&l, &n, stdin) != -1) {
         char *l_copy = my_strdup(l);
         table = my_str_to_word_array(l_copy);
         key = my_getnbr(table[0]);
         data = table[1];
         linked_list_t *find_f = NULL;

        if (data != NULL) {
            if (data[0] == 'D') {
                find_f = my_find_node(list, key);
                if (find_f == NULL) {
                 my_putstr("-1 \n");
                } else {
                    my_putstr(find_f->value);
                    my_delete_nodes(&list, key);
                }
            } else {
                find_f = my_find_node(list, key);
                my_putnbr(key);
                my_putstr("\n");
                if (find_f == NULL) {
                    list = my_push_front_to_list(data, key, list);
                } else {
                    find_f->value = data;
                }
            }
        }

        if (data == NULL) {
            find_f = my_find_node(list, key);
             if (find_f == NULL) {
                 my_putstr("-1 \n");
             } else {
                 my_putstr(find_f->value);
             }
        }
         free(l_copy);
     }
}

int main(void){
    principal();
    return (0);
}