#ifndef __MY__
#define __MY__

typedef struct linked_list
{
char *value;
int key;
struct linked_list *next;
} linked_list_t;

//Prototypes

linked_list_t *my_push_front_to_list(char *value, int key, linked_list_t *list);
linked_list_t *my_find_node(linked_list_t *list, const int data_ref);
void my_delete_nodes(linked_list_t **list, const int data_ref);
int my_getnbr(char *l);
char *my_strdup(const char *src);
char **my_str_to_word_array(char *str);
void my_putstr(char *str);
void my_putnbr(int nb);
void my_putchar(char c);

#endif