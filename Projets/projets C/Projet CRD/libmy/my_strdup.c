/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
** 		No file there, just an etna header example
*/

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


char *my_strdup(const char *src)
{
    char *dest;
    int	len;
    int	i;
    len = 0;
    i = 0;
    if (src == NULL) {
        return (NULL);
    }
    while (src[len] != '\0')
        len++;
    dest = (char*)malloc(sizeof(*dest) * len + 1);
    if (dest == NULL) {
        return (NULL);
    }
    while (i < len)
    {
        dest[i] = src[i];
        i++;
    }
    dest[i] = '\0';
    return (dest);
}