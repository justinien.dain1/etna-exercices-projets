/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
**              No file there, just an etna header example
*/

#include <string.h>
#include <unistd.h>
#include <stdio.h>

int my_getnbr(char *l)
 {
    int n=0;
    int i=0;

    if (l == NULL) {
        return (0);
    }

    while(l[i] != '\0' && l[i] != '\n'){
        n = n * 10 + (l[i] - 48);
        i++;
    }
    return(n);
 }

// void my_putchar(char c);
// int my_getnbr(const char *str)
// {
// int i = 0;
//     int n = 0;
//     int neg = 1;

//     while ((str[i] == '+' || str[i] == '-') && str[i] != '\0') {
//         if (str[i] == '-') {
//             neg = neg *= -1;
//         }
//         i++;
//     }
//     while ((str[i] >= '0' && str[i] <= '9') && str[i] != '\0') {
//             n = (n * 10) + str[i] - '0';
//             i++;
//     }
//     return (n * neg);
// }
