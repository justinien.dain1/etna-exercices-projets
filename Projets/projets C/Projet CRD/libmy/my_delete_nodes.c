#include <stdio.h>
#include <stdlib.h>
#include "../my.h"

linked_list_t *my_pop_front_from_list(linked_list_t *list)
{

  linked_list_t *element;
  element = malloc(sizeof(linked_list_t));
  if(element == NULL) {
    return (NULL);
  }
  element = list->next;
  free(list);
  list = NULL;
  return element;
};

void my_delete_nodes(linked_list_t **list, const int data_ref) {

  if(list == NULL || *list == NULL)
  {
    return;
  }

  if ((*list)->key == data_ref)
  {
    *list = my_pop_front_from_list (*list);
    my_delete_nodes(&(*list), data_ref);
  }
  else {
    my_delete_nodes(&(*list)->next,data_ref);
  }
};
