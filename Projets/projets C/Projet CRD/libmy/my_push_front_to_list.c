#include <stdio.h>
#include <stdlib.h>
#include "../my.h"



linked_list_t *my_push_front_to_list(char *value, int key, linked_list_t *list)
{
  linked_list_t *element;

  element = malloc(sizeof(*element));

  if(element == NULL)
  {
    return list;
  }

  element->value = value;
  element->key = key;
  
  if (list == NULL)
  {
    element->next = NULL;
  }
  else
  {
    element->next = list;
  }

  return element;
};
