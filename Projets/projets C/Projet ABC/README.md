Le projet ABC est le premier projet que l'on ait eu à faire il consistait à 
écrire une fonction qui écrira l'alphabet le nombre de fois défini dans la fonction ( max 52 ) 
chaque fois que l'alphabet sera complet il fautsauter une ligne et décaler le début d'une lettre.

Exemple :

print_adc_square(2);

vas faire :

abcdefghijklmnopqrsduvwxyz<br/>

bcdefghijklmnopqrsduvwxyza

Pour l'utiliser il faut cloner le dossier, aller dans le fichier abc.c et :

- A la fin dans l'appel de fonction choisir le nombre de ligne que l'on veut ( max 52)
- fermer le fichier
- faire GCC abc.c
et enfin 
- ./a.out dans le terminal 
