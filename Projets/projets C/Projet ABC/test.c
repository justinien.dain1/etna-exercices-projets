/*
** ETNA PROJECT, 2021
** RUSH ABC
** File description:
**              No file there, just an etna header example
*/

#include <unistd.h>
#include <stdio.h>
void print_abc_square(int n)
{
        char c;
        int m=97; 	// a minuscule
        for(int i=0;i<n;i++){ 	// boucle pour le nombre de ligne
                int t=i; 	// stock la ligne actuelle
                if(i>26){ 	// si il y a plus de 26 ligne retourner à a
			i=i-26; 	// pour revenir a et incrémenter
                }

                for(c = 97+i; c <= 122; c++) 	// boucle pour ecrire l'alp$
                {
                        	//printf("\033[1;31m"); test couleurs
                        write(1,&c,1); 	// ecrit la lettre
                }
                int f=0;
                while(f<i){ 	// boucle pour la fin de l'alphabet
                        int y=m+f;	 // y= et f est incrémenter pour faire$
                        write(1,&y,1); 	// ecrit la lettre
                        f++; 	// incrémente f
                }
                i=t;	 // i reprend sa ligne
                write(1,"\n",1); 	// retour à la ligne
        }
}
int main(){
print_abc_square(52);
}

