#!/bin/bash
#time_before_doom.sh

lien=$1
 
a=$(curl -s $lien | cut -d'"' -f6)
b=$(curl -s $lien | cut -d'"' -f10)
c=$(curl -s $lien | cut -d'"' -f14)
d=$(curl -s $lien | cut -d'"' -f18)
e=$(curl -s $lien | cut -d'"' -f22)

temps=$(echo $d*1000/$e | bc)

#echo $temps

y=$(echo $temps/60/60/24 | bc)

h=$(date -d@$temps  -u +%H)
m=$(date -d@$temps  -u +%M)
s=$(date -d@$temps  -u +%-S)

echo The Angry Moon is located at lat: $c, lng: $b.
echo It is 384400km away from us, but is it going to fall at a speed of 1484m/s.
echo We have exactly $y days, $h hours, $m minutes and $s seconds to react. 
