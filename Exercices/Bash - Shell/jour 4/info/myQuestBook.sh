#!/bin/bash

file=$3
idc=$2

now=$(date +"%y-%m-%d" )
nnow=$(date -d $now -u +%s)

while IFS=";" read -r ide name description quest_type completion_state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
if [[ $ide -eq $idc ]]; then
if [[ $completion_state -eq 1 ]]; then
qdt=$(date -d $start_date -u +%s)
diff=$(($nnow - $qdt))
diff2=$(($diff/60/60/24))
echo "========================================"
echo "#$id $name ($quest_type quest)"
echo "========================================"
echo "Given by $quest_giver" $diff2 days ago.
echo "Complete since the $end_date ($diff2 days ago)."
echo "======================================="
echo "Goal: $description"
if [[ -z $reward ]]; then
echo "reward: ---"
else
echo "reward: $reward"
fi
fi
fi
fi
first_line=1
done < $file

while IFS=";" read -r ide name description quest_type completion_state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
if [[ $ide -eq $idc ]]; then
if [[ $completion_state -eq 0 ]]; then
qdt=$(date -d $start_date -u +%s)
diff=$(($nnow - $qdt))
diff2=$(($diff/60/60/24))
echo "======================================="
echo "#$id $name ($quest_type quest)"
echo "========================================"
echo "Given by $quest_giver today." 
echo "Currently ongoing"
echo "======================================="
echo "Goal: $description"
echo "Reward: $reward"

fi
fi
fi
first_line=1

done < $file


