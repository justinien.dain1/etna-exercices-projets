#!/bin/bash

first_line=0
file=$2
echo "=== Ongoing ==="
while IFS=";" read -r id name description quest_type state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
if [[ $state -eq 0 ]]; then
echo "#$id $name"
fi
fi
first_line=1
done < $file


echo "=== Completed ==="
unset completion_state
while IFS=";" read -r id name description quest_type state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
if [[ $state -eq 1 ]]; then
echo "#$id $name"
fi
fi
first_line=1
done < $file

echo "=== Failed ==="
unset completion_state
while IFS=";" read -r id name description quest_type state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
if [[ $state -eq 2 ]]; then
echo "#$id $name"
fi
fi
first_line=1
done < $file
echo "None"
