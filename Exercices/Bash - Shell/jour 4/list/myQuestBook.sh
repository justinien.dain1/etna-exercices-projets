#!/bin/bash


first_line=0
file=$2
while IFS=";" read  id name description quest_type completion_state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
echo "#$id $name"
fi
first_line=1

done < $file

