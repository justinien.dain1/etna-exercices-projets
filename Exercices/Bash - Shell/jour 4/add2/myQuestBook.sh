#!/bin/bash

nom=$3
type=$7
desc=$5
completion=$9
giver=${11}
start_dates=${13}
end_dates=${15}
rewards=${17}
file=${18}
n=0


start_dates=$(date -d "$start_dates" +%s)
echo "debut date ; " $start_dates

echo $end_dates
date -d "$end_dates" +%s
start_dates=$(date -d "$end_dates" +%s)
echo $end_dates



completion=$( echo "$completion" | tr '[:upper:]' '[:lower:]')
first_line=0
while IFS=";" read  id name description quest_type completion_state quest_giver start_date end_date reward 
do 
if [[ first_line -ne 0 ]]; then
first_line=1
fi
n=$(($n + 1))
done < $file
n=$(($n + 1))

if [[ "$completion" == "ongoing" ]]; then
completion=$((0))
fi

if [[ "$completion" == "completed" ]] || [[ "$completion" == "complete" ]]; then
completion=$((1))
fi

if [[ "$completion" == "fail" ]] || [[ "$completion" == "failed" ]]; then
completion=$((2))
fi

if [[ "$type" == "0" ]]; then
type="main"
fi

if [[ "$type" == "1" ]]; then
type="main"
fi

if [[ "$type" == "2" ]]; then
type="secondary"
fi


echo $n";"$nom";"$type";"$completion";"$desc";"$giver";"$start_dates";"$end_dates";"$rewards 



