#!/bin/bash

nom=$3
type=$5
desc=$7
completion=$9
giver=${11}
start_dates=${13}
end_dates=${15}
rewards=${17}
file=${18}
n=0

first=1
while IFS=";" read  id name description quest_type completion_state quest_giver start_date end_date reward 
do
if [[ $first -ne 0 ]]; then

n=$(($n + 1))
fi
first=1
done < $file

n=$(($n + 1))

echo $n";"$nom";"$type";"$desc";"$completion";"$giver";"$start_dates";"$end_dates";"$rewards >> questbook_data.csv 



