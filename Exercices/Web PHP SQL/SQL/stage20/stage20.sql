SELECT CONCAT(types.id, " - ", COUNT(*)) AS nb FROM pokemon 
INNER JOIN pokemon_types ON pokemon.id = pokemon_types.pokemon_id
INNER JOIN types ON types.id = pokemon_types.type_id
GROUP BY types.id;