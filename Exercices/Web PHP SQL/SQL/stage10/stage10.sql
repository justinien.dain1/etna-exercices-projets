SELECT egg_groups.identifier AS TypeOeuf
FROM pokemon
INNER JOIN pokemon_egg_groups ON pokemon_egg_groups.species_id = pokemon.species_id
INNER JOIN egg_groups ON egg_groups.id = pokemon_egg_groups.egg_group_id
WHERE pokemon.identifier = "noctowl";