SELECT (
SELECT DISTINCT(moves.identifier)
FROM pokemon
LEFT JOIN pokemon_moves ON pokemon_moves.pokemon_id = pokemon.id
INNER JOIN moves ON moves.id = pokemon_moves.move_id 
WHERE pokemon.identifier="feraligatr" AND moves.identifier="hydro-pump") AS Attaques
FROM dual;