/*
** ETNA PROJECT, 25/10/2021 by goncal_a
** my_add_in_sort_list.c
** File description:
**       function that that sorts a list in ascending order by comparing data, node-to-node
*/
#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"

linked_list_t *my_push_front_to_list(int value, linked_list_t *list)
{
  linked_list_t *element;

  element = malloc(sizeof(*element));

  if(element == NULL)
  {
    return element;
  }

  element->data = value;
  
  if (list == NULL)
  {
    element->next = NULL;
  }
  else
  {
    element->next = list;
  }

  return element;
};

void my_add_in_sorted_list(linked_list_t **begin, int data)
{
//----------------------------------------------------------------//
 if(*begin == NULL){
   return;
 }
    
    linked_list_t *current =*begin;
    linked_list_t *after =*begin;
    
        after=current->next;
        
        int tmp=0;
        

    while(current->next !=NULL){
        
        after = current->next;
        
    
        if(current->data > after->data){
            tmp=current->data;
            current->data = after->data;
            after->data = tmp;
            current=*begin;
           
        
        }else      
            current=current->next;
          
    }

   //----------------------------------------------------------------// 
     linked_list_t *element1;

  element1 = malloc(sizeof(*element1));
  element1->data = data;
  current=*begin;
     if (current->data >= element1->data)
    {
              
        int tmp1;
        element1->next = current->next;
        current->next = element1;
        
        tmp1 =  current->data;
        current->data = element1->data;
        element1->data = tmp1;
         
        return; 
    }
    
    while(current->next !=NULL){
      after=current->next;
      if (current->data < element1->data && after->data >= element1->data){

           element1->next = current->next; 
           current->next = element1;
                     

      }else if (after->next == NULL && element1->data >= after->data)
        {
          
           after->next = element1;
           element1->next = NULL;
           return;
        }
      current = current->next;
    } 
}

int main ()
{
  
    linked_list_t *list;
  
  
  list = NULL;
  
  list =my_push_front_to_list(22, list);
  list =my_push_front_to_list(6,list);
  list =my_push_front_to_list(16,list);
  list =my_push_front_to_list(-7,list);
  list =my_push_front_to_list(34,list);
  
  my_add_in_sorted_list(&list,1);
  my_add_in_sorted_list(&list,-12);
  my_add_in_sorted_list(&list,9);
  my_add_in_sorted_list(&list,6);
  my_add_in_sorted_list(&list,80);
  
  my_add_in_sorted_list(&list,95);
  
  
    while (list != NULL)
  {
    
      printf("%d  ",list->data);
      list = list->next;
  }
}




