#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"



linked_list_t *my_pop_back_from_list(linked_list_t *list)
{
        if (list == NULL){
                return list;
        }

	if (list->next == NULL){
		free(list);
		list = NULL;
		return list;
	}

    linked_list_t *temp =list;
	linked_list_t *befor =list;

	while (temp->next != NULL){
		befor = temp;
		temp=temp->next;
	}

	befor->next = NULL;
	free(temp);
	temp=NULL;
	return list;
};

