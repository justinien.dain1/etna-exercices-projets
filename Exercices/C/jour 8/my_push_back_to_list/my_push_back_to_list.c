#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"


linked_list_t *my_push_back_to_list(int value, linked_list_t *list)
{
 linked_list_t *element;

  element = malloc(sizeof(*element));

  if(element == NULL)
  {
    return element;
  }

  element->data = value;
  element->next = NULL;

  if (list == NULL)
  {
    return element;
  }
  linked_list_t *temp;
  temp=list;

  while(temp->next != NULL){
	temp=temp->next;
  }

  temp->next = element;
  return element;
};



