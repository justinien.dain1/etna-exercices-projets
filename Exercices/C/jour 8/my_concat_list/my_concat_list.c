#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../include/my_list.h"


void my_concat_list(linked_list_t **begin1, linked_list_t *begin2)
{
    
    linked_list_t *tmp = *begin1;

    while(tmp->next)
        tmp = tmp->next;
    tmp->next = begin2;
}