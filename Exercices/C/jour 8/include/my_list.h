#ifndef __LIST__H__
#define __LIST__H__

typedef struct linked_list_t
{
  int data;
  struct linked_list_t *next;
} linked_list_t;

linked_list_t *my_push_back_to_list(int value, linked_list_t *list);
linked_list_t *my_pop_back_from_list(linked_list_t *list);
void my_sort_list(linked_list_t **begin);
void my_concat_list(linked_list_t **begin1, linked_list_t *begin2);
void my_add_in_sorted_list(linked_list_t **begin, int data);
void my_merge(linked_list_t **begin1, linked_list_t *begin2);

#endif

