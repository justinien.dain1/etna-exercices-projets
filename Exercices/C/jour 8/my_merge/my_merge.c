#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../include/my_list.h"

linked_list_t *my_push_front_to_list(int value, linked_list_t *list)
{
  linked_list_t *element;

  element = malloc(sizeof(*element));

  if(element == NULL)
  {
    return element;
  }

  element->data = value;
  
  if (list == NULL)
  {
    element->next = NULL;
  }
  else
  {
    element->next = list;
  }

  return element;
};


void my_merge(linked_list_t **begin1, linked_list_t *begin2)
{

    if(*begin1 == NULL){
        return;
    }

    if(begin2 == NULL){
        return;
    }
    
    linked_list_t *tmp1 = *begin1;

    while(tmp1->next)
        tmp1 = tmp1->next;
    tmp1->next = begin2;




    
    linked_list_t *current =*begin1;
   
    linked_list_t *after =*begin1;
    
        after=current->next;
        
        int tmp=0;
        

    while(current->next !=NULL){
        after = current->next;
        if(current->data > after->data){
                tmp=current->data;
            current->data = after->data;
            after->data = tmp;
                current=*begin1;
        }else      
            current=current->next;
    }
}

int main ()
{
  
    linked_list_t *list1;
    linked_list_t *list2;
  
  
  list1 = NULL;
  list2 = NULL;
  
  list1 =my_push_front_to_list(15, list1);
  list1 =my_push_front_to_list(22,list1);
  list1 =my_push_front_to_list(9,list1);
  list1 =my_push_front_to_list(12,list1);
  list1 =my_push_front_to_list(-3,list1);

  list2 =my_push_front_to_list(33, list2);
  list2 =my_push_front_to_list(16,list2);
  list2 =my_push_front_to_list(25,list2);
  list2 =my_push_front_to_list(8,list2);
  list2 =my_push_front_to_list(17,list2);
  
    my_merge(&list1,list2);

    printf("Liste 1 :");
   while (list1 != NULL)
  {
      printf("%d  ",list1->data);
      list1 = list1->next;
  }
  
  printf("\n");
/*
    printf("Liste 2 :");
    while (list2 != NULL)
  {
      printf("%d  ",list2->data);
      list2 = list2->next;
  }
  */
}