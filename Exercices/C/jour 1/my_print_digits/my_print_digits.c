
/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
** 		No file there, just an etna header example
*/


#include <unistd.h>
#include <stdio.h>


void my_putchar(char c);

void my_print_digits()
{
    char c;

    for(c = 48; c <= 57; c++)

	{
        my_putchar(c);

	}
}

