#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"

int my_list_size(const linked_list_t *list)
{
int size = 0;

  if(list != NULL)
  {
    while(list != NULL)
    {
      ++size;
      list = list->next;
    }
  }

  return size;
}
