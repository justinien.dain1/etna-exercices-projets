#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"



linked_list_t *my_find_node(linked_list_t *list, const int data_ref)
{
 if(list == NULL)
  {
    return NULL;
  }

  linked_list_t *current = list;

  while(current != NULL)
  {
    if(current->data == data_ref)
    {
      return current;
    }
    current=current->next;
  }
  return NULL;
}

