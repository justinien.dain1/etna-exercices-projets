#include <stdio.h>
#include <stdlib.h>
#include "../include/my_list.h"



linked_list_t *my_push_front_to_list(int value, linked_list_t *list)
{
  linked_list_t *element;

  element = malloc(sizeof(*element));

  if(element == NULL)
  {
    return element;
  }

  element->data = value;
  
  if (list == NULL)
  {
    element->next = NULL;
  }
  else
  {
    element->next = list;
  }

  return element;
};
