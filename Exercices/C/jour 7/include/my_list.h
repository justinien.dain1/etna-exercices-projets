#ifndef __LIST__H__
#define __LIST__H__

typedef struct linked_list_t
{
  int data;
  struct linked_list_t *next;
} linked_list_t;

linked_list_t *my_push_front_to_list(int value, linked_list_t *list);
linked_list_t *my_pop_front_from_list(linked_list_t *list);
int my_list_size(const linked_list_t *list);
linked_list_t *my_find_node(linked_list_t *list, const int data_ref);
void my_delete_nodes(linked_list_t **list, const int data_ref);

#endif
