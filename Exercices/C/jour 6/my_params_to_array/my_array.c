#include <stdio.h>
#include <stdlib.h>



char **allocate_my_tab(char *str)
{
    // cette fonction de permet de copier un string dans tableau

    int size = 0; // taille en nombre 
    char ** tab; // pointeur de pointeur de tableau  
    int col = 0; // colen nombre 

    for (int i=0; str[i] != '\0'; i++){
        if (str[i] == ' ')
        size++;
    } 
    // cette boucle permet de compter la taille string avec de l'espace. 
    
    tab = malloc(sizeof (char *) * (size + 1) + 1);
    size = 0;
    for (int i = 0; str[i] != '\0'; i++){
        size++; 
        if(str[i] ==  ' ' || str[i + 1] == '\0'){
            tab[col] = malloc(sizeof(char) * (size +1));
            size = 0;
            col++; 
        }

    }
    return tab;
}

char **my_str_to_word_array(char *str)
{
    int n=0;  
    int j=0; 

    char **tab = allocate_my_tab(str);

    for (int i = 0; str[i] != '\0'; i++){
        if (str[i] != ' ')
            tab[n][j++] = str[i]; 
        else {
            tab[n][j++] = '\0';
            n++;
            j = 0;
        }
    } 
    return (tab);
}

//int main(){

//char **e = my_str_to_word_array("bonjouyr gzquisgd gdsjhqd ddd");

//  for(int i = 0; e[i]; ++i)
//  {
  //  for(int a = 0; e[i][a] != '\0'; ++a)
   // {
//      printf("%c", e[i][a]);
  //  }
//    printf("\n");
  //}


//}
