/*
** ETNA PROJECT, 2021
** RUSH ABC
** File description:
** 		No file there, just an etna header example
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_struct.h"


int strlen(char *str){
	int i;
	i=0;
	while (str[i]!='\0'){
	i++;
	return(i);
	}
}

char strdup(*src){

	char *dest;
	int i;
	int len;
	len = 0;
	i = 0;
	while (src[len] != '\0')
		len++;
	dest = (char*)malloc(sizeof(*dest) * len + 1);
	while (i < len)
	{
		dest[i] = src[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

char **allocate_my_tab(char *str)
{
    // cette fonction de permet de copier un string dans tableau

    int size = 0; // taille en nombre
    char ** tab; // pointeur de pointeur de tableau
    int col = 0; // colen nombre

    for (int i=0; str[i] != '\0'; i++){
        if (str[i] == ' ')
        size++;
    }
    // cette boucle permet de compter la taille string avec de l'espace.

    tab = malloc(sizeof (char *) * (size + 1) + 1);
    size = 0;
    for (int i = 0; str[i] != '\0'; i++){
        size++;
        if(str[i] ==  ' ' || str[i + 1] == '\0'){
            tab[col] = malloc(sizeof(char) * (size +1));
            size = 0;
            col++;
        }

    }
    return tab;
}

char **my_str_to_word_array(char *str)
{
    int n=0;
    int j=0;

    char **tab = allocate_my_tab(str);

    for (int i = 0; str[i] != '\0'; i++){
        if (str[i] != ' ')
            tab[n][j++] = str[i];
        else {
            tab[n][j++] = '\0';
            n++;
            j = 0;
        }
    }
    return (tab);
}
struct info_param *my_params_to_array(int ac,char**av){
	int i;
	i=0;
	while (i < ac){
	info_param[i].length = strlen(av[i]);
	info_param[i].str = av[i];
	info_param[i].copy = strdup(av[i]);
	info_param[i].word_array = my_str_to_word_array(av[i]); // celle qui manque
	i++;
	}
	info_param[i].str =0;
	return(info_param);
}
