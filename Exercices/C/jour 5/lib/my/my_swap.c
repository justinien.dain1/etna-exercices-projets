/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
** 		No file there, just an etna header example
*/


#include <unistd.h>
#include <stdio.h>


void my_putchar(char c);
void my_swap(int *a, int *b)
{
	int y;
	y=*a;
	*a=*b;
	*b=y;

}
