/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
** 		No file there, just an etna header example
*/


#include <unistd.h>
#include <stdio.h>


void my_putchar(char c);
void my_putstr(char *str)
{
	int		i;
	i = 0;

	while (str[i] != '\0')
	{
		my_putchar(str[i]);
		i++;
	}

}
