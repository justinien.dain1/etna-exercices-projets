/*
** ETNA PROJECT, $YEAR
** $NAME_OF_THE_PROJECT
** File description:
** 		No file there, just an etna header example
*/

#include <string.h>
#include <unistd.h>
#include <stdio.h>




int	my_putchar(char c);

void	my_putnbr(int nb)
{

	if (nb < 0)
	{
		my_putchar('-');
		nb = -nb;
	}
	if (nb / 10)
	{
		my_putnbr(nb/10);
	}
	my_putchar('0' + nb % 10);

}

